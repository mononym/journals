[![Awesome](https://awesome.re/badge-flat.svg)](https://awesome.re)

**ARCHITECTURE**
+ [Arq](https://www.cambridge.org/core/journals/arq-architectural-research-quarterly) - Architecture Research Quarterly [RSS]
+ [EdA Esempi di Architettura](http://esempidiarchitettura.it/sito/journal/)
+ [Frontiers of Architectural Research](https://www.sciencedirect.com/journal/frontiers-of-architectural-research) [RSS]
+ [HypoTesis](http://hipo-tesis.eu/serienumerada/index.php/ojs/index) [RSS]
+ [JAE](https://www-tandfonline-com.ezproxy.ulb.ac.be/rjae20) - Journal of Architectural Education [RSS]
+ [Les Cahiers de la recherche architecturale, urbaine et paysagère](https://journals.openedition.org/craup/) - Revue développant une histoire réflexive du fonctionnement des dominations sociales dans leurs dimensions politiques, économiques et culturelles [RSS]

**ARCHITECTURE ET SCIENCES INFORMATIQUES**
+ [DNArchi](http://dnarchi.fr/) - Design Numérique Architecture [RSS]
+ [IJAC](https://journals.sagepub.com/home/JAC) - International Journal of Architectural Computing [RSS]

**ARTISTIC RESEARCH**
+ [Art&Research](http://www.artandresearch.org.uk/v2n2/v2n2editorial.html) - A Journal of Ideas, Contexts and Methods [Newsletter]
+ [JAR](https://www.jar-online.net/) - Journal for Artistic Research
+ [Journal of Black Mountain College Studies](http://www.blackmountainstudiesjournal.org/)
+ [MaHKUscript](https://www.mahkuscript.com/) - Journal of Fine Art Research [RSS]
+ [PARSE](https://parsejournal.com/) [Newsletter]

**ARTS**
+ [Arts et sciences](https://www.openscience.fr/Arts-et-sciences)
+ [Ligeia](https://www.cairn.info/revue-ligeia.htm) - Dossiers sur l'Art

**ARTS, SCIENCES HUMAINES ET SOCIALES**
+ [Daedalus](https://www.mitpressjournals.org/loi/daed) [RSS]

**DESIGN**
+ [Artifact](https://www.intellectbooks.com/artifact-journal-of-design-practice) - Journal of Design Practice
+ [Design Science](https://www.cambridge.org/core/journals/design-science) [RSS]
+ [She Ji](https://www.sciencedirect.com/journal/she-ji-the-journal-of-design-economics-and-innovation) - The Journal of Design, Economics, and Innovation [RSS]

**DESSIN ET REPRÉSENTATION D'ARCHITECTURE**
+ [DISEGNARECON](http://disegnarecon.univaq.it/ojs/index.php/disegnarecon)
+ [diségno](https://disegno.unioneitalianadisegno.it/index.php/disegno)

**HISTOIRE**
+ [Bulletin de la Sabix](http://journals.openedition.org/sabix) - Bulletin valorisant le patrimoine scientifique de l'École polytechnique [RSS]
+ [Livraisons d’histoire de l’architecture](http://journals.openedition.org/lha) - Revue de jeunes chercheurs en histoire de l’architecture [RSS]

**HISTOIRE DE LA CONSTRUCTION**
+ [Ædificare](https://classiques-garnier.com/aedificare.html) [RSS]
+ [Construction History](http://www.constructionhistory.co.uk/publications/construction-history-journal/)

**HISTOIRE DES SCIENCES ET TECHNIQUES**
+ [Archives Internationales d'Histoire des Sciences](http://www.brepols.net/Pages/ShowProduct.aspx?prod_id=IS-9782503582535-1)
+ [Artefact](http://journals.openedition.org/artefact) - Revue de recherches sur la technique et la matérialité des pratiques dans les sociétés humaines [RSS]
+ [BJHS](https://www.cambridge.org/core/journals/british-journal-for-the-history-of-science) - The British Journal for the History of Science
+ [e-Phaïstos](http://journals.openedition.org/ephaistos) - Revue d’histoire des techniques [RSS]
+ [Histoire & Mesure](https://journals-openedition-org/histoiremesure/) [RSS]
+ [HoST](https://content.sciendo.com/view/journals/host/host-overview.xml?tab_body=overview) - Journal of History of Science and Technology [RSS]
+ [Isis](https://www.journals.uchicago.edu/toc/isis/current) - A Journal of the History of Science Society [RSS]
+ [NTM Zeitschrift für Geschichte der Wissenschaften, Technik und Medizin](https://www.springer.com/journal/48/)
+ [Nuncius](https://brill.com/view/journals/nun/nun-overview.xml) - Journal of the Material and Visual History of Science

**IMAGE**
+ [img journal](https://img-journal.unibo.it/) - Interdisciplinary scientific publication that explores the interconnections between the different fields of images, imagery, and imagination. [RSS]
+ [textimage](https://www.revue-textimage.com/intro1.htm) - Revue d'étude du dialogue texte-image

**PATRIMOINE**
+ [eikonocity](http://www.serena.unina.it/index.php/eikonocity/index) [RSS]

**TECHNOLOGIE**
+ [TAD](https://tadjournal.org/) - Technology | Architecture + Design [Newsletter]
+ [Technologies](https://www.mdpi.com/journal/technologies) [RSS]
+ [Technophany](http://journal.philosophyandtechnology.network/) – Journal for Philosophy and Technology [Newsletter]

**THÉORIE ET ANALYSE**
+ [Appareil](http://journals.openedition.org/appareil) - Revue transdisciplinaire de théorie et d’analyse des appareils [RSS]
+ [Footprint](https://journals.open.tudelft.nl/footprint/index) - Delft Architecture Theory Journal [RSS]
